//  Section - Using Aggregate Method 

db.fruits.aggregate([
  {$match:{onSale: true}}
]);

db.fruits.aggregate([
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

db.fruits.aggregate([
  {$match:{onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

db.fruits.aggregate([
  {$match:{onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
  {$project: {_id: 0}}
]);

db.fruits.aggregate([
  {$match:{onSale: true}},
  {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
  {$sort: {total: -1}}
]);

db.fruits.aggregate([
  {$group: {_id: "$origin", kinds: {$sum: 1}}}
]);

db.fruits.aggregate([
  {$unwind: "$origin"}
]);

db.fruits.aggregate([
  {$unwind: "$origin"},
    {$group: {_id:"$origin", kinds: {$sum:1}}}
]);


var owner = ObjectId();

// referral of ids
db.owners.insert({
  _id: owner,
  name: "John Smith",
  contact: "09123456789"
});

// design structure, make data structure
db.suppliers.insert({
  name: "ABC fruits",
  contact: "09123456789",
  owner_id: ObjectId("62d54a9975e01230239ec4fa")
});

db.suppliers.insert({
  name: "DEF fruits",
  contact: "0912345679",
  address: [
    {street: "123 San Jose St.", city: "Manila"},
    {street: "367 Gil Puyat", city: "Manila"}
  ]
});

// ^^ Inserting an object inside supplier collection
var supplier = ObjectId();
var branch = ObjectId();

db.supplier.insert({
_id: supplier,
name: "GHI Fruits",
contact: "0917000000", 
branches: [      
  branch
]
});

db.branch.insert({
  name: Mike Greene,
  address: ,
  city: ,
  supplier_id: 
});


